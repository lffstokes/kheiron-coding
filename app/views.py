from app import app
from flask import url_for, request, render_template,  redirect
from calculators.infix_calculator import InfixCalculator
from calculators.prefix_calculator import PrefixCalculator


@app.route("/", methods=['GET', 'POST'])
def index():

    if request.method == "POST":
        infix = request.form.get("infix")
        prefix = request.form.get("prefix")

        infix_answer, prefix_answer = None, None
        if infix:
            in_calc = InfixCalculator(terminal_mode=False)
            infix_answer = in_calc.calculate_from_string(infix)
        if prefix:
            pre_calc = PrefixCalculator(terminal_mode=False)
            prefix_answer = pre_calc.calculate_from_string(prefix)

        return render_template("home.html",
                               infix_answer=infix_answer,
                               infix_question=infix,
                               prefix_answer=prefix_answer,
                               prefix_question=prefix)

    return render_template("home.html")