import sys
import os
import argparse
sys.path.append(os.getcwd())

from calculators.prefix_calculator import PrefixCalculator
from calculators.infix_calculator import InfixCalculator


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Run a calculator')
    parser.add_argument("--calc", dest='calculator', type=str,
                        help="Use either infix or prefix")
    args = parser.parse_args()

    if args.calculator == "infix":
        in_calc = InfixCalculator()
    elif args.calculator == "prefix":
        pre_calc = PrefixCalculator()
    else:
        print("No such calculator")
