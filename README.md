# Prefix and Infix calculator

I used python 3.6.8

For both tasks, it was assumed every number, bracket or operator was separated by a space.

To run the calculators directly in the terminal, do   
`python run_calculator.py --calc infix`  
or   
`python run_calculator.py --calc prefix`  
for infix and prefix methods respectively.  

To run the calculator via flask, first install flask using  
`pip install flask`  
and run the application using:
`python run.py`
Visit http://127.0.0.1:5000/ and enter equations!


