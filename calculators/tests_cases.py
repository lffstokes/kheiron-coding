import unittest
from calculators.infix_calculator import InfixCalculator
from calculators.prefix_calculator import PrefixCalculator


class TestCalculator(unittest.TestCase):
    def setUp(self):
        self.prefix_calc = PrefixCalculator(terminal_mode=False)
        self.infix_calc = InfixCalculator(terminal_mode=False)

    def test_0_prefix(self):
        result = self.prefix_calc.calculate_from_string("3")
        self.assertEqual(result, 3, "Prefix test 0")

    def test_1_prefix(self):
        result = self.prefix_calc.calculate_from_string("+ 1 2")
        self.assertEqual(result, 3, "Prefix test 1")

    def test_2_prefix(self):
        result = self.prefix_calc.calculate_from_string("+ 1 * 2 3")
        self.assertEqual(result, 7, "Prefix test 2")

    def test_3_prefix(self):
        result = self.prefix_calc.calculate_from_string("+ * 1 2 3")
        self.assertEqual(result, 5, "Prefix test 3")

    def test_4_prefix(self):
        result = self.prefix_calc.calculate_from_string("- / 10 + 1 1 * 1 2")
        self.assertEqual(result, 3, "Prefix test 4")

    def test_5_prefix(self):
        result = self.prefix_calc.calculate_from_string("- 0 3")
        self.assertEqual(result, -3, "Prefix test 5")

    def test_6_prefix(self):
        result = self.prefix_calc.calculate_from_string("/ 3 2")
        self.assertEqual(result, 1, "Prefix test 6")

    def test_7_prefix_extra_space(self):
        result = self.prefix_calc.calculate_from_string(" / 3 2 ")
        self.assertEqual(result, 1, "Prefix test 7")

    def test_8_wrong_format(self):
        result = self.infix_calc.calculate_from_string("( 5")
        self.assertEqual(result, "Incorrect equation format", "Infix test 8")

    def test_9_infix_simple(self):
        result = self.infix_calc.calculate_from_string("3 + 5")
        self.assertEqual(result, 8, "Infix test 9")

    def test_10_infix_order(self):
        result = self.infix_calc.calculate_from_string("5 + 2 * 3")
        self.assertEqual(result, 11, "Infix test 10")

    def test_11_infix(self):
        result = self.infix_calc.calculate_from_string("( 1 + 2 )")
        self.assertEqual(result, 3, "Infix test 11")

    def test_12_infix(self):
        result = self.infix_calc.calculate_from_string("( 1 + ( 2 * 3 ) )")
        self.assertEqual(result, 7, "Infix test 12")

    def test_13_infix_parentheses(self):
        result = self.infix_calc.calculate_from_string("( ( 1 * 2 ) + 3 )")
        self.assertEqual(result, 5, "Infix test 13")

    def test_14_infix_parentheses(self):
        result = self.infix_calc.calculate_from_string("( ( ( 1 + 1 ) / 10 ) - ( 1 * 2 ) )")
        self.assertEqual(result, -1, "Infix test 14")


if __name__ == "__main__":
    unittest.main()
