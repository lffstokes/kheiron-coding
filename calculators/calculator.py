from abc import ABC, abstractmethod


class Calculator(ABC):
    def __init__(self, terminal_mode):
        self._operations = {"+": self.add,
                            "-": self.subtract,
                            "*": self.multiply,
                            "/": self.divide}
        self._terminal_mode = terminal_mode
        self._command_text = None

    @staticmethod
    def add(x, y):
        x, y, = float(x), float(y)
        return x + y

    @staticmethod
    def subtract(x, y):
        x, y, = float(x), float(y)
        return x - y

    @staticmethod
    def multiply(x, y):
        x, y, = float(x), float(y)
        return x * y

    @staticmethod
    def divide(x, y):
        x, y, = float(x), float(y)
        return x / y

    def is_operator(self, value):
        if value in self._operations:
            return True
        else:
            return False

    @abstractmethod
    def calculate(self,  parts):
        pass

    def calculate_from_string(self, string):
        parts = string.strip().split()
        return self.calculate(parts)

    def get_input(self):
        prefix_equation = input(self._command_text)
        parts = prefix_equation.strip().split()
        value = self.calculate(parts)
        print(value)
        if self._terminal_mode:
            self.get_input()
