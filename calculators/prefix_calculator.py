from calculators.calculator import Calculator


class PrefixCalculator(Calculator):
    def __init__(self, terminal_mode=True):
        super().__init__(terminal_mode)
        self._command_text = "Enter calculation using prefix notation\n"
        if self._terminal_mode:
            self.get_input()

    def rolling_window(self, parts):
        for i, part in enumerate(parts[:-2]):
            win = parts[i:i+3]
            yield i, win

    def calculate(self, parts):
        if len(parts) == 1:
            try:
                val = int(parts[0])
                return val
            except ValueError:
                return "Incorrect equation format"

        # loop until we find operator with two integers following
        for i, win in self.rolling_window(parts):
            if self.is_operator(win[0]) and not self.is_operator(win[1]) and not self.is_operator(win[2]):
                # found first operation
                value = self._operations[win[0]](win[1], win[2])
                new_parts = parts[:i] + [value] + parts[i+3:]
                return self.calculate(new_parts)
        return "Incorrect equation format"


if __name__ == "__main__":

    pf = PrefixCalculator()
