from calculators.calculator import Calculator


class InfixCalculator(Calculator):
    def __init__(self, terminal_mode=True):
        super().__init__(terminal_mode)
        self._parentheses = ["(", ")"]
        self._command_text = "Enter calculation using infix notation\n"
        if self._terminal_mode:
            self.get_input()

    def rolling_window(self, parts):
        for i, part in enumerate(parts[:-2]):
            win = parts[i:i+3]
            yield i+1, win

    def is_number(self,  part):
        if not self.is_operator(part) and part not in self._parentheses:
            return True
        else:
            return False

    def calculate_clean_parentheses(self, i, parts):
        value = self._operations[parts[i]](parts[i - 1], parts[i + 1])

        # check for parentheses
        if (i - 2) >= 0 and (i + 1) <= len(parts):
            # if exist remove and pass on
            if parts[i - 2] == "(" and parts[i + 2] == ")":
                new_parts = parts[:i - 2] + [value] + parts[i + 3:]
                return self.calculate(new_parts)

        new_parts = parts[:i - 1] + [value] + parts[i + 2:]
        return self.calculate(new_parts)

    def calculate(self,  parts):
        if len(parts) == 1:
            try:
                val = int(parts[0])
                return val
            except ValueError:
                return "Incorrect equation format"

        # somehow need to add priority to * and /
        # get indices * and /
        idx = None
        for i, win in self.rolling_window(parts):
            if win[1] == "*" or win[1] == "/":
                if self.is_number(win[0]) and self.is_number(win[2]):
                    idx = i
                    break

        if idx is not None:
            return self.calculate_clean_parentheses(idx, parts)

        for i, win in self.rolling_window(parts):
            if self.is_operator(win[1]) and self.is_number(win[0]) and self.is_number(win[2]):
                return self.calculate_clean_parentheses(i, parts)

        return "Incorrect equation format"


'''
    def calculate(self, parts):
        if len(parts) == 1:
            return parts[0]

        # somehow need to add priority to * and /
        # get indices * and /
        idxs = []
        for i, win in self.rolling_window(parts):
            if win[1] == "*" or win[1] == "/":
                if win[0] not in self._parentheses and win[1] not in self._parentheses:
                    idxs.append(i)
        print(idxs)
        
        # loop until we find operator between two integers following
        for i, win in self.rolling_window(parts):
            if self.is_operator(win[1]) and self.is_number(win[0]) and self.is_number(win[2]):
                value = self._operations[win[1]](win[0], win[2])

                # check for parentheses
                if (i-2) >= 0 and (i+1) <= len(parts):
                    # if exist remove and pass on
                    if parts[i-2] == "(" and parts[i+2] == ")":
                        new_parts = parts[:i-2] + [value] + parts[i+3:]
                        return self.calculate(new_parts)
                else:
                    new_parts = parts[:i-1] + [value] + parts[i+2:]
                    return self.calculate(new_parts)

'''

if __name__ == "__main__":

    incalc = InfixCalculator()
